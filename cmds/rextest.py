#! python3
#!/usr/bin/python
# -*- coding: utf-8 -*-
import discord
import urllib.request
import urllib.error
import urllib.parse
import json
from discord.ext import commands
import time
import keys

class rextest(commands.Cog, name="Rextest"):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def test(self , ctx, language, *, input : str):#This is like the command input, so for this one you have to say "say (input)" and it will output the input
        """Usage: {}test <language #> <code> language #s, visit rextester.com/main""".format(keys.getPrefix())
        load1 = await ctx.send("**Currently Starting up**")
        try:
            time.sleep(1)
            load2 = await load1.edit(content="~~Currently Starting up~~\n**Running Script**")
            url = 'http://rextester.com/rundotnet/api'
            postdata = urllib.parse.urlencode({
                'LanguageChoice': language,
                'Program': "",
                'Input': input,
                'CompilerArgs': "",
                })
            postdatabytes = str.encode(postdata)
            req = urllib.request.Request(url, postdatabytes)
            response = urllib.request.urlopen(req)
            output = response.read()
            #print 'API response: ' + output
            response_decoded = json.loads(output)
            warns = response_decoded["Warnings"]
            er = response_decoded["Errors"]
            re = response_decoded["Result"]
            st = response_decoded["Stats"]
            #print "Decoded JSON:"
            #print response_decoded
            await load1.edit(content="~~Currently Starting up\nRunning Script~~")
            await ctx.send("**Result**\n```{}```\n**Warns**\n```{}```\n**Errors**\n```{}```\n**Stats**\n```{}```".format(re, warns, er, st))

        except Exception as e:
            await ctx.send("I have ran into a error :x:")
            raise

def setup(bot):
    bot.add_cog(rextest(bot))
