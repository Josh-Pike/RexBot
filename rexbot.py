#! python3
#!/usr/bin/python
# -*- coding: utf-8 -*-
print("Loading Discord Bot....")
import discord
from discord.ext import commands
import sys, traceback
from os import listdir
from os.path import isfile, join
import keys

def get_prefix(bot, message):

    prefixes = ['{}'.format(keys.getPrefix())]

    return commands.when_mentioned_or(*prefixes)(bot, message)

bot = commands.Bot(command_prefix=get_prefix, description='REXBOT by R3dfox')

initial_extensions = ['cmds.rextest']

if __name__ == '__main__':
    for extension in initial_extensions:
        bot.load_extension(extension)
        print("Extension loaded")


@bot.event
async def on_ready():

    print("Rexbot is now fully loaded")


bot.run(keys.getToken(), bot=True, reconnect=True)
